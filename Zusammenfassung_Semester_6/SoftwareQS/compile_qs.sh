#!/bin/bash

pdflatex -interaction=nonstopmode SoftwareQS
pdflatex -interaction=nonstopmode SoftwareQS
pdflatex -interaction=nonstopmode SoftwareQS

rm SoftwareQS.blg
rm SoftwareQS.bbl
rm SoftwareQS.aux
rm SoftwareQS.lof
rm SoftwareQS.log
rm SoftwareQS.lol
rm SoftwareQS.toc
rm SoftwareQS.out

open SoftwareQS.pdf