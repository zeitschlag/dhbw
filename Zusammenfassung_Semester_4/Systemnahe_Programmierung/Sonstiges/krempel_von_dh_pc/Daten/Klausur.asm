;Macros zusammengestellt von Tobias Kohs bla bla 
;Stand: 27.04.2012 15:02
;Ich �bernehme keine Garantie f�r Korrektheit
;Wenn verwendet ggf. was dazuschreiben: Macros entstanden in Zusammenarbeit 
;mit Komilitonen im Rahmen der Vorlesung Rechnertechnik 2-2 oder so.
;oder einfach die macros anpassen ;)

;gibt einen String aus, der �bergeben wird
%MACRO AUSGABE_STRING 1 
	PUSH EDX
	PUSH EAX
	MOV DX,%1 
	MOV AH,9 
	INT 21H
	POP EAX
	POP EDX
%ENDMACRO 


Org 100H
Bits 16

section .CODE
start:
AUSGABE_STRING string1
AUSGABE_STRING formfeed
xor cl,cl
xor bl,bl
mov di,ziel1
mov cl,30
	
EINGABESCHLEIFE1:
	mov ah,1
	int 21h
	cmp al,13
	je ende1
	stosb
	inc bl
		loop EINGABESCHLEIFE1
		
ende1: 
cmp bl,5
jl zukurz1
cmp bl,10
jg zulang1
mov al,"$"
stosb
AUSGABE_STRING ziel1
AUSGABE_STRING formfeed
jmp start2


zukurz1:
AUSGABE_STRING string2
jmp start
zulang1:
AUSGABE_STRING string3
jmp start

start2:
AUSGABE_STRING string1
AUSGABE_STRING formfeed
xor cl,cl
xor bl,bl
mov di,ziel2
mov cl,30

EINGABESCHLEIFE2:
	mov ah,1
	int 21h
	cmp al,13
	je ende2
	stosb
	inc bl
		loop EINGABESCHLEIFE2
		
ende2: 
cmp bl,5
jl zukurz2
cmp bl,10
jg zulang2
mov al,"$"
stosb
AUSGABE_STRING ziel2
jmp weiter

zukurz2:
AUSGABE_STRING string2
jmp start2
zulang2:
AUSGABE_STRING string3
jmp start2


weiter:
mov di,Ausgabe
mov si,ziel1
mov cl,10

Schleife:
	mov al,[si] 
	cmp al,"$"
	je weiter2
	stosb
	inc si
		loop Schleife
		
weiter2:
mov si,ziel2
mov cl,10

Schleife2:
	mov al,[si] 
	cmp al,"$"
	je weiter3
	stosb
	inc si
		loop Schleife2
		
weiter3:
mov al,"$"
stosb
AUSGABE_STRING formfeed
AUSGABE_STRING formfeed
AUSGABE_STRING zusammen
AUSGABE_STRING Ausgabe
mov cl,30
xor bl,bl
mov si,Ausgabe

Schleife3:
	mov al,[si]
	cmp al,"$"
	je ende
	sub al,41H
	cmp al,19H
	jl rechne
	back:
	inc si
		loop Schleife3
rechne:
inc bl
jmp back

ende:
AUSGABE_STRING formfeed
AUSGABE_STRING string4
add bl,30H
mov dl,bl
mov ah,02
int 21h
AUSGABE_STRING string5
mov ah,4ch
int 21h

section .Data

formfeed db 10,13,"$"


ziel1 db "****************************************",10,13,"$"

ziel2 db "****************************************",10,13,"$"

Ausgabe db "hi",10,13,"$"

string1 db "Bitte geben sie einen String ein:$",10,13,"$"

string2 db "Der String ist zu kurz",10,13,"$"

string3 db "Der String ist zu lang",10,13,"$"

string4 db "Der String enthaelt $"

string5 db " Grossbuchstaben",10,13,"$"


zusammen db "Zusammengefuegter String:",10,13,"$"

