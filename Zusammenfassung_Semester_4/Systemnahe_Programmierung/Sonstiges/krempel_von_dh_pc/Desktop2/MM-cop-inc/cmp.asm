org 100h
bits 16

%macro ausgabe 1
MOV DX,%1
MOV AH,9
int 21H
%endmacro

section .code

mov di,string1
mov si,string2

ausgabe string1
ausgabe linefeed
ausgabe string2
ausgabe linefeed
ausgabe linefeed

mov cx,15

schleife:
mov dl,byte[di]
cmp dl,byte[si]
jne nicht
inc di
inc si
loop schleife

ausgabe gle
jmp ende

nicht:
ausgabe ngl
ausgabe linefeed
mov dx,16D
sub dx,cx
add dx,30H
cmp dx,39H
jle next
add dx,7
next:
mov al,dl
mov ah,2H
int 21H


ende:
mov ah,4CH
int 21H


section .data
string1 db "hallooo$"
linefeed db 10,13,"$"
string2 db "hallooi$"
ngl db "Strings unterscheiden sich an: $"
gle db "Strings sind identisch!$"