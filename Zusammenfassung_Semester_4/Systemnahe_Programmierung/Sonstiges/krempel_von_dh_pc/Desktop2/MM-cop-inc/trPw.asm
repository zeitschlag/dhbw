ORG 100H		; Programm Passwort 
Bits 16		; h�chstens dreimaliger Versuch ein Passwort richtig einzugeben
%macro ausgabe 1		; String Ausgabemacro
mov dx, %1		
mov ah,9		
int 21H		
%endmacro		


%macro laengencheck 2		; Macro L�ngencheck �bernimmt die Referenz einer Zeichenkette und
mov si,%1	; Anfangsadresse	; speichert den L�ngenwert in die ebenfalls �bergebene Referenz der
mov bx,si	; auf allg 16-Bit Var.	; relevanten Variablen
%%weiter:		
cmp byte[si],"$"	; Abbruchkriterium	
je %%rechne	; wenn ja, dann Berechnung	
inc si	; sonst inkrementiere SI	
jmp %%weiter		
%%rechne:		
mov ax,si	; Endadresse der Var nach AX	; Berechnung der 
sub ax,bx	; Diff von BX und AX nach AX	
mov byte[%2],al	; der 8-Bit-Wert in Var speichern	
%endmacro		


%macro stringvgl 2		; �bernimmt 2 versch. Strings
push ax	; sichere AX	
mov al,byte[p_len]	; beide L�ngen nach AX	
mov ah,byte[g_len]		
cmp al,ah	; L�ngenvergleich	
jne %%raus			; wenn ungleich, dann kein weiterer Vergl n�tig	
mov si, %1			;sonst: Referenzen nach Si	
mov di, %2			; und DI	
mov cl, byte[p_len]    		; L�nge ist gleich, dann Zeichen vergleichen		
xor al,al			; cl belegt, wegen LOOP	
%%loop1:			; Anfangslabel loop	
mov al,byte[si]			; byteweiser Vgl der Adresseninhalte 8 Bit	
mov bl,byte[di] 		

cmp al,61H
jl %%isNoChar
cmp al,7AH
jg %%isNoChar
sub al,32D
%%isNoChar:

cmp al,bl		
jne %%raus			; wenn ungleich, dann Schleife verlassen	
inc si				; source und	
inc di				; destination index erh�hen	
loop %%loop1			; Schleifenende	
mov byte[gleich],0		; wenn Schleife vollst�ndig durchlaufen, dann alle Zeichen gleich	
jmp %%ende			; dann den Inhalt der Var auf 0 setzen	
%%raus:	; und springen	
mov byte[gleich],1		; sonst Inhalt der Var auf 1 setzen	
%%ende:		
%endmacro			; Ende Macro	


section .code			; Beginn Code-Section	
laengencheck gespeichert,g_len	; L�nge des gespeicherten Codewortes feststellen	
aussen:				; �u�ere Schleife (Anzahl der Versuche!)
mov byte[notchar],0	
mov byte[p_len],0		; L�nge des einzugebenden Passwortes auf 0	
inc byte[versuch]		; Var versuch wird inkrementiert	
cmp byte[versuch],3		; vgl mit Wert 3	
jg fertig			; wenn gr��er, dann finale Fehleingabe	
mov cx,10			; Festgelegte Obergrenze f�r Anzahl von Zeichen	
mov si,passwort			; Referenz des einzugebenden Strings nach si	
ausgabe meldung1		
mov dl,byte[versuch]		; Anzahl der Versuche	
add dl,30H			; wird	
mov ah,2			; am Bildschirm	
int 21H	; gezeigt	
ausgabe formfeed		; Zeilenvorschub	
loop1:	; Schleifenbeginn	
mov ah,8			; Eingabe Zeichen ohne Echo	
int 21H		
cmp al,13	; war es ENTER?	
je raus				; wenn ja, dann springe raus
cmp cx,1
jg weiter
;ausgabe formfeed
;ausgabe meldung7
;ausgabe formfeed
jmp loop1
weiter:

push ax	; Sichere AX	
mov ah,2			; am Bildschirm	
mov dl,'*'			; ist nur der *	
int 21H				; zu sehen	
pop ax				; Restauriere AX	


cmp al,41H
jl isNoChar
cmp al,5AH
jg noCapitalChar
noCapitalChar:
cmp al,61H
jl isNoChar
cmp al,7AH
jg isNoChar
sub al,32D
jmp wasChar
isNoChar:
inc byte[notchar]
wasChar:

mov byte[si],al			; eingegebenes Zeichen an Adresse von SI speichern
inc si				; inkrementiere SI	
loop loop1	; Schleifenende	
raus:	; Schleifenausstieg bei ENTER	
mov byte[si],"$"	; Endekennung anh�ngen	
mov bh,byte[notchar]
cmp bh,2
jge stimmt
ausgabe formfeed
ausgabe meldung6
jmp aussen
stimmt:
		
laengencheck passwort,p_len	; L�nge des eingegebenen Passwortes feststellen
mov bh,byte[p_len]	; beide L�ngen
cmp bh,byte[g_len]	; vergleichen
jne nein	; wenn ungleich ist kein detaillierter Zeichenvgl n�tig
stringvgl gespeichert, passwort	; sonst Zeichenvergleich �ber Macro
cmp byte[gleich],0	; ist die gespeicherte Var 0 ==> Symbol f�r Gleichheit
jne nein	; wenn nicht, springe
ausgabe formfeed	
ausgabe meldung4	; Passwort richtig - zugang gestattet
jmp Ende	; dann Ende
nein:	; sonst 
ausgabe formfeed	
ausgabe meldung2	; Meldung "Passwort falsch"
jmp aussen	; springe unbedingt zum n�chsten Versuch
fertig:	; Label
ausgabe meldung5	; Meldung "Passwort 3 mal falsch eingegeben
ausgabe formfeed	; keine weitere Eingabe gestattet
Ende:	
mov ah, 4cH	; Beendigungscode
int 21H	
Section .DATA	
	
p_len db 0  ; passwortl�nge wird eingegeben	
g_len db 0  ; gespeicherte L�nge	
gespeichert db "TeSt55A34$"	
passwort db "**********$"	
meldung1 db "Eingabe Passwort; Versuch: $"	
meldung2 db "Passwort falsch!",10,13,"$"	
meldung4 db "Passwort richtig - Sie haben Zugang zum System $"	
meldung5 db "Passwort 3 mal falsch eingegeben - Sie sind nicht autorisiert zum System $"
meldung6 db "Geben Sie ein Passwort mit mindestens 2 Nichtbuchstaben ein! $"	
meldung7 db "Passwort zu lang, Esel! $"	
	
versuch db 0	
notchar db 0
formfeed db 10,13,"$"	
blank db " $"	
	
gleich db 2;   gleich=1 ungleich =0	
