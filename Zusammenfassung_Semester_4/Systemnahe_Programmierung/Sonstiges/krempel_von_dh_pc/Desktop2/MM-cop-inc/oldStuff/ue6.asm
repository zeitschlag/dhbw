org 0x100
bits 16

SECTION .CODE

START:
	MOV cx, var1
	MOV [adr],cx
	MOV cx, var2
	MOV [adr+2],cx
	MOV cx, var3
	MOV [adr+4],cx
	MOV CL,byte[adr+2]
	MOV CH,byte[adr+3]
	
SECTION .DATA
var1 dw 1234H
var2 dw 4321H
var3 dw 5678H

adr: TIMES 3 dw 0u