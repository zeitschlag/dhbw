%macro bitaus 1

push ax
mov cl,%1
mul cl,4
shl ax,cl
mov cl,12
shr ax,cl
and ax,000fH
add al,30H
mov dl,al
mov ah,2
int 21h
pop ax

%endmacro

org 100H
bits 16

section .code
mov ax,3827H

mov bl,3H
mov bh,00H

BEG:
bitaus bl
dec bx
cmp bl,bh
jnz BEG

mov ah,4CH
int 21h

