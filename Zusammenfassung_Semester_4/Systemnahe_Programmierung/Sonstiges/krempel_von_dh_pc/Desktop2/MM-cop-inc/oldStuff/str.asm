org 100H
Bits 16

%macro ausgabe 1
mov dx,%1
mov ah,9H
int 21H
%endmacro

section .code
ausgabe aufforderung
mov di,estring
mov cx,15

schleife:
	mov ah,1H
	int 21H
	cmp al,13
	je fertig
	mov byte[di],al
	inc byte[laenge]
	loop schleife

fertig:
mov al, "$"
mov byte[di],al

ausgabe linefeed
ausgabe estring
ausgabe linefeed
ausgabe len

mov dl, byte[laenge]
cmp dl,9

jg hex

add dl,30H
mov ah, 2H
int 21H

jmp ende

hex:
add dl,37H
mov ah,2H
int 21H

ende:
ausgabe linefeed
mov ah,4CH
int 21H

section .data
aufforderung db "Geben Sie ein: ",10,13,"$"
estring db "*************$"
len db "Laenge:$"
laenge db 0
linefeed db 10,13,"$"