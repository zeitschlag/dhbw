org 100H
bits 16

%macro ausgabe 1
mov dl,[%1]
add dl,30H
mov ah,2
int 21H
%endmacro

section .code

mov cx,10

schleife:

mov ah,0H
mov al,[ihwe]
mov bl,2
div bl
cmp ah,0
jne weiter
ausgabe ihwe
weiter:
inc byte[ihwe]

loop schleife

mov ah,4ch
int 21H

section .data
ihwe db 0