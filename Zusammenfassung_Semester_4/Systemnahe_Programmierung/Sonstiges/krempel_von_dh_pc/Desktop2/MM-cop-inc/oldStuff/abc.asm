org 0x100
bits 16

section .code
mov bx,0078H
mov cl,4
shl bx,cl
shr bl,cl
add bh,30H
add bl,30H

mov dl,bl
mov ah,2
int 21h
mov dl,bh
int 21h

mov ah,4ch
int 21h
