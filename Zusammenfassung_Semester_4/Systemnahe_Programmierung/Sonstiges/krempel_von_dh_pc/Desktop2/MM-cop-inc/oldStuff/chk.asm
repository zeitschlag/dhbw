%macro ausgabe 1
MOV DX,%1
MOV AH,9
int 21H
%endmacro


org 0x100
bits 16

section .code

mov AL,99H
mov BL,AL

ausgabe bat
rcl BL,1
JC NOTNOT
call NICHT
NOTNOT:
ausgabe loaded

ausgabe bil
rcl BL,3
JC NOTNOT2
call NICHT
NOTNOT2:
ausgabe hoc

ausgabe usb
rcl BL,1
JC NOTNOT3
call NICHT
NOTNOT3:
ausgabe bel

ausgabe cop
rcl BL,3
JC NOTNOT4
call NICHT
NOTNOT4:
ausgabe akt

JMP ENDE

NICHT:
ausgabe nic
CLC
ret

ENDE:

mov ah,4CH
int 21h


section .data

bat db 10,13,"Batterie ist $"
nic db "nicht $"
loaded db "geladen!$"
cop db 10,13,"Der Coprozessor ist $"
akt db "aktiv!$"
usb db 10,13,"Die USB-Schnittstelle ist $"
bel db "belegt!$"
bil db 10,13,"Der Bildschirm ist $"
hoc db "hochaufloesend!$"