
%macro ausgabe 1
MOV DX,%1
MOV AH,9
int 21H
%endmacro

org 100H
bits 16

section .code
START:
	ausgabe welcome
	jmp EINGABE1

EINGABE1:
	ausgabe eingabe

	; erste Ziffer
	MOV AH,1
	INT 21H
	
		
	; Gueltigkeitspruefung
	CMP AL,39H
	JG FEHLER1
	CMP AL,30H
	JL FEHLER1
	
	MOV [var1],AL


	; zweite Ziffer
	MOV AH,1
	INT 21H

	; Gueltigkeitspruefung
	CMP AL,39H
	JG FEHLER1
	CMP AL,30H
	JL FEHLER1
	
	ADD AL,[var1]
	MOV [var1],AL

	JMP EINGABE2

EINGABE2:

	ausgabe eingabe

	; erste Ziffer
	MOV AH,1
	INT 21H
	
	; Gueltigkeitspruefung
	CMP AL,39H
	JG FEHLER2
	CMP AL,30H
	JL FEHLER2
	
	MOV [var2],AL


	; zweite Ziffer
	MOV AH,1
	INT 21H
		
	; Gueltigkeitspruefung
	CMP AL,39H
	JG FEHLER2
	CMP AL,30H
	JL FEHLER2
	
	; speichern
	ADD AL,[var2]
	MOV [var2],AL

	JMP PRUEFUNG

PRUEFUNG:
	MOV AL,[var1]
	MOV BL,[var2]
	CMP AL,BL
	JE EQUAL
	JG VAR1GREATER
	JL VAR2GREATER
	
	EQUAL:
		ausgabe equal
		JMP ENDE

	VAR1GREATER:
		ausgabe firstBigger
		JMP ENDE

	VAR2GREATER:
		ausgabe secondBigger
		JMP ENDE


ENDE:
	MOV AH,4CH
	INT 21H 

FEHLER1:
	ausgabe error1
	JMP EINGABE1

FEHLER2:
	ausgabe error1
	JMP EINGABE2


section .data
error1 db 10,13,"Fehlerhafte Ziffer eingegeben $"
eingabe db 10,13,"Eingabe: $"
welcome db 10,13,"Hallo! $"
equal db 10,13,"Die Zahlen sind gleich$"
firstBigger db 10,13,"Var1 ist groesser$"
secondBigger db 10,13,"Var2 ist groesser$"

var1 db 0
var2 db 0