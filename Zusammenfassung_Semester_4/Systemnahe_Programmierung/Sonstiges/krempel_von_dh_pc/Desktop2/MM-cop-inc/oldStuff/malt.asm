	
	org 100H
	bits 16
	
	%macro ausgabe 1
	mov dx,%1
	mov ah,9
	int 21H
	%endmacro
	section .code
	; Schleife zum �berpr�fen von 8 Bits
	; mit dem Test-Befehl
	; und Einspeichern in ein Array
	; Sp�ter Ausgabe des Arrays
	mov bl,1B
	mov al,1AH
	mov cx,8D
	mov si,array1
	add si,7
	schleife:
	test al,bl
	jz yes
	mov byte[si],1
	jmp no
	yes:
	mov byte[si],0
	no:
	push cx
	mov cl,1
	shl bl,cl
	pop cx
	dec si
	loop schleife
	
	; Ausgabeschleife f�r Array
	ausgabe bit
	mov si,array1
	mov ah,2
	mov cx,8
	aus:
	mov dl,byte [si]
	add dl,30H
	int 21H
	inc si
	loop aus
	
	mov ah,4cH
	int 21H
	
	section .data
	bit db "Bit-Inhalt des AL-Registers: ",10,13,"$"
	;nicht   db ". Bit ist nicht gesetzt ",10,13,"$"
	array1: Times 8 db 2
