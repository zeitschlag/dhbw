ORG 100H
Bits 16
SECTION .CODE
Start: ; Programm-Anfangs-Label
MOV ax,CS ; Initialisiere Datensegment per
MOV DS,ax
mov bl, 'A' ; Buchstabe A nach bl
mov [array1],bl ; speichere an Adresse array1
inc bl ; erh�he BL (Buchstabe B)
mov [array1+1],bl ; speichere an Adresse array1+1
inc bl ; erh�he BL (Buchstabe C)
mov [array1+2],bl ; speichere an Adresse array1+2
inc bl
mov [array1+3],bl
mov Ah,02h
mov dx,[array1]
int 21H
mov dx,[array1+1]
int 21H
mov dx,[array1+2]
int 21H
mov dx,[array1+3]
int 21H
MOV AH, 4CH ; Programmende
int 21H

mov cx,3
MARKE

LOOP MARKE

SECTION .Data
array1: Times 5 db 0 ; Byte-Array mit 0 initialisiert