ORG 100H
Bits 16

%macro ausgabe 1
mov ah,9
mov dx,%1
int 21h
%endmacro
section .code
ausgabe person
ausgabe strasse
ausgabe vorschub
ausgabe stadt
mov ah,4cH
int 21H
section .data
person db "Yannick Bock",10,13,"$"
strasse db "Fritz Frey",10,13,"$"
stadt db "blabla Heidelberg",10,13,"$"
vorschub db 10,13,"$"
