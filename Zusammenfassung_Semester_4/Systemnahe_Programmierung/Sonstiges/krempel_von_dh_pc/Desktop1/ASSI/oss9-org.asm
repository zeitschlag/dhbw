;
;Programmname: Oss9.ASM ***********************************************************************
;*************************************************************************************************************************
;***************************************************************
;* Programm : Vergleichen verschiedener Operanden und Sprungmarken *
;* *
;* *
;***************************************************************
ORG 100H
Bits 16
section .CODE ; Beginn Codesegment
Start:
M1:
xor ax,ax ; setze AX und BX auf 0
xor bx,bx ; k�rzer als MOV BX,0
mov al,[op1] ; Inhalt Operand 1 nach AL
mov bl,[op2] ; Inhalt Operand 2 nach BL
cmp al,bl ; vergleiche AL und BL
ja erstergroesser ; wenn erster gr��er dann Sprung
jmp zweitergroesser
erstergroesser:
mov dx,Meldung1 ; Ausgabefunktion Zeichenketten
mov ah,9
int 21H
weiter: xor ax,ax ; setze AX und BX auf 0
xor bx,bx
mov al,[op3] ; Inhalt Operand 3 nach AL
mov bl,[op4] ; Operand 4 nach BL
cmp al,bl ; vergleiche beide
jl zweitergroesser ; wenn 2. kleiner
jmp weiter1 ; unbedingter Sprung
zweitergroesser: ; Sprungmarke
mov dx, Meldung2
mov ah,9
int 21H
weiter1:
xor ax,ax ;Anweisungen f�r gleich grosse
xor bx,bx ; Inhalte
mov ax,[op5]
mov bx,[op6]
cmp ax,bx
je gleichgross
jmp ENDE
gleichgross: ; Sprungmarke
mov dx, Meldung3
mov ah,9
int 21H
ENDE: ;Sprungmarke Programmende
mov ah,4CH
int 21H
section .Data
Meldung1 db 10,13,"erster Operand ist groesser$" ; Bildschirmausgaben
Meldung2 db 10,13,"zweiter Operand ist groesser$"
Meldung3 db 10,13,"beide sind gleich gross$"
op1 db 5 ; Operanden
op2 db 3 ;
op3 db "A" ; hier mit unterschiedl. ASCII
op4 db "X"
op5 dw 1234H ; WORD-Variablen
op6 dw 1234H