org 0x100
bits 16

%macro stringaus 1
MOV DX,%1
MOV AH,9
int 21H
%endmacro

SECTION .CODE

START:
	stringaus vorname
	stringaus name
	stringaus leer
	stringaus plz
	stringaus ort

	MOV AH, 4CH
	INT 21H

SECTION .DATA

vorname db "Malte $"
name db "Jauer",10,13,"$"
leer db 10,13,"$"
plz db "06660$"
ort db " Hier",10,13,"$"