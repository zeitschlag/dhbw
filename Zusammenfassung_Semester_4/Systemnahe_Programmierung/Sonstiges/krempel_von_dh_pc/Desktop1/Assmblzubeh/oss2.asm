ORG 100H
Bits 16
SECTION .CODE
Start: ; Programm-Anfangs-Label
MOV ax,CS ; Initialisiere Datensegment per
MOV DS,ax ; Ringtausch (beide Teile in der gleichen Section)
MOV bx, 15A3H ; lade bx mit beliebigem Hexwert
MOV word[variable], bx ; Inhalt von bx der Variablen variable zuweisen
MOV AH, 4CH ; Programmende
int 21H
SECTION .Data
variable dw 0 ; Definition einer WORT-Variablen