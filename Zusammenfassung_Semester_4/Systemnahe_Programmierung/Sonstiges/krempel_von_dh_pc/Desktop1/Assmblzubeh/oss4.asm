ORG 100H
Bits 16
SECTION .CODE
MOV ax,CS ; Initialisiere Datensegment per
MOV DS,ax ; Ringtausch
MOV cx, variable ; lade Offset der Variablen-Adresse
MOV Bl,byte[variable] ; lade niedrigwertigen 8-Bit-Inhalt
MOV BH,byte[variable+1] ; lade höherwertigen 8-Bit-Inhalt
mov [niedrig],bl ; kopiere Inhalt in 8-Bit-Var.
mov [hoch],bh ; kopiere 
MOV AH, 4CH ; Programmende
int 21H
SECTION .Data
variable dw 32ABH ; Definition einer WORT-Variablen
niedrig db 0
hoch db 0 ;Inhalt in 8-Bit-Var.