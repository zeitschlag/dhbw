%macro ausgabe 1
	MOV DX, %1
	MOV AH, 9
	INT 21H
%endmacro

org 0x100
bits 16

SECTION .CODE
M1:
	ausgabe formfeed
	ausgabe meldung1  
	ausgabe quelle
	ausgabe formfeed

	MOV SI, quelle		;Referenz des Quellstrings nach SI
	MOV DI, ziel		;Referenz des Zielstrings nach DI

COPY:					;Label für Schleifenanfang
	CMP byte[SI], "$"	;War das adressierte Zeichen ein $ ?
		JZ RAUS	
		
	MOV DL, byte[SI]	;Kopiertes Zeichen in 8-Bit-Register		
	MOV byte[DI], DL	;Von dort aus an Byte-Adresse im Zielstring
	INC SI				;Erhöhe Source-Index
	INC DI				;Erhöhe Destination-Index
	JMP COPY

RAUS:
	MOV byte[DI], "$"	;Hänge Endekennung an
						;nach Ziel. Durch den rep-Befehl werden alle Zeichen kopiert
						
	ausgabe formfeed
	ausgabe meldung2  
	ausgabe ziel
	
ENDE:
	MOV AH, 4CH
	INT 21H

SECTION .DATA
meldung1	db "Ausgabe des Quellstrings: ", "$"
meldung2	db "Ausgabe des kopierten Zielstrings: ", "$"
formfeed	db 10, 13, "$"
quelle		db "Das ist ein vordefinierter String$"
ziel		db "**********************************************************************$" 

