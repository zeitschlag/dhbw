org 0x100
bits 16

SECTION .CODE
M1:
	MOV SI, string1		;String-Offset nach SI
	
AUSGABE:
	MOV DL, [SI]		;Dereferenzierung von SI
	
	CMP DL, "$"			;War das Zeichen die Endekennung?
		JE ENDE			;Wenn ja, dann springe zum ENDE-Label
		
	MOV AH, 2			;Ausgabe des Zeichens
	INT 21H
	INC SI				;Inkrementiere Index-Register
	JMP AUSGABE              

ENDE:
	MOV AH, 4CH
	INT 21H

SECTION .DATA
string1		db "Ich bin ein String $"
formfeed	db 10, 13, "$"
