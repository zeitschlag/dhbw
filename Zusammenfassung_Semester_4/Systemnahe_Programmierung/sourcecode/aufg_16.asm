%macro ausgabe 1
	MOV DX, %1
	MOV AH, 9
	INT 21H
%endmacro

org 0x100
bits 16

SECTION .CODE
M1:
	ausgabe meldung1
	mov di, estring1
	
EINGABE:
	MOV AH, 01H		
	INT 21H
	STOSB			
	
	CMP AL, 13		
		JNE EINGABE
		
	MOV AL, "$"		
	STOSB			
	
FERTIG:
	ausgabe formfeed
	ausgabe estring1

ENDE:
	MOV AH, 4CH
	INT 21H

SECTION .DATA
formfeed	db 10, 13, "$"
estring1	db "....................$"
