org 0x100
bits 16

SECTION .CODE
M1:
	MOV AX, CS				;Initialisiere Datensegment per Ringtausch
	MOV DS, AX
	MOV CX, variable		;Lade Offset der Variablen Adresse
	MOV AH, 4CH				;Programmende
	INT 21H

SECTION .DATA
variable dw 32ABH			;Definition einer WORT-Variablen
