org 0x100
bits 16

SECTION .CODE
M1:
	MOV SI, string1		;String-Offset nach SI
	
AUSGABE:
	LODSB				;LODS-Befehl lädt Zeichen in AL-Register
	MOV DL, AL			;Kopieren nach DL zur Ausgabe
	
	CMP DL, "$"			;War das Zeichen die Endekennung?
		JE ENDE			;Wenn ja, dann springe ans Ende
	
	;Zeichenausgabe
	MOV AH, 2
	INT 21H
	
	JMP AUSGABE              

ENDE:
	MOV AH, 4CH
	INT 21H

SECTION .DATA
string1		db "Ich bin ein String $"
formfeed	db 10, 13, "$"