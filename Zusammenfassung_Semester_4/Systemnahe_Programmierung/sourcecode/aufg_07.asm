org 0x100
bits 16

SECTION .CODE
MOV AX, 3578H			;AX mit Anfangswert laden
PUSH AX

XCHG AH, AL				;Vertausche AH und AL	   	
XOR AH, AH				;Nulle AH-Register
PUSH AX					;AX auf Stack legen
AND AL, 11110000B		;Maskiere AL
MOV Cl, 4
SHR AL, CL				;In AL 4 Bits nach rechts (identisch mit Division /10hex)

;Zeichenausgabe über DL-Register	
ADD AL, 30H       
MOV DL, AL
MOV AH, 2
INT 21H

POP AX					;Letzten gespeicherten Wert von AX von Stapel
AND AL, 00001111B		;Maskiere AL

;Zeichenausgabe über DL-Register
ADD AL,30H   
MOV DL,AL
MOV AH,2
INT 21H

POP AX					;Vorletzten gepeicherten Wert vom Stack holen
XOR AH, AH				;Nulle AH
PUSH AX					;Lege aktuellen AX-Inhalt auf Stack	
AND AL, 11110000B		;Maskiere AL
MOV CL, 4				;Lade Anzahl der zu schiebenden Zeichen
SHR AL, CL				;Schiebe Anzahl von cl Zeichen in AL nach rechts

;Zeichenausgabe über DL-Register
ADD AL, 30H
MOV DL, AL
MOV AH, 2
INT 21H

POP AX					;Letzten gesicherten AX-Inhalt vom Stapel
AND AL, 00001111B		;Maskiere AL

;Zeichenausgabe über DL-Register; 
ADD AL, 30H
MOV DL, AL
MOV AH, 2
INT 21H

;Programmende
MOV AH, 4CH
INT 21H