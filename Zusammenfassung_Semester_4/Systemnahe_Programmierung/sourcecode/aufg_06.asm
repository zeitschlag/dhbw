rech_betrag	EQU 12
mwst		EQU 19
teiler		EQU 100

org 0x100
bits 16

SECTION .CODE
M1:
	MOV BX, mwst			;Weise die Konstante mwst zu
	MOV AX, rech_betrag		;Lade AX mit Konstanten rech_betrag
	MUL BX					;Multipliziere AX mit BX, Ergebnis nach AX
	MOV CX, teiler			;Konstante teiler wird cx zugewiesen
	DIV CX					;Ergebnis (Vorkommastelle) steht in AX,
							;die Nachkommastelle in DX

ENDE:
	MOV AH, 4CH
	INT 21H
