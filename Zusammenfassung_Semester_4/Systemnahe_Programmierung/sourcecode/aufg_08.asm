org 0x100
bits 16

SECTION .CODE
;Ausgabe Überschrift
MOV AH, 9
MOV DX, ueberschrift
INT 21H
               
;Ausgabe Variable kapitel1
MOV AH, 9
MOV DX, kapitel1
INT 21H
               
;Ausgabe Variable kapitel2
MOV AH, 9
MOV DX, kapitel2
INT 21H

;Programmende
MOV AH, 4CH
INT 21H

SECTION .DATA
ueberschrift	db "Assembler-Programmierung", 10, 13, "$"
kapitel1		db "Kapitel 1: Registeroperationen", 10, 13, "$"
kapitel2		db "Kapitel 2: Stack-Operationen", 10, 13, "$"
