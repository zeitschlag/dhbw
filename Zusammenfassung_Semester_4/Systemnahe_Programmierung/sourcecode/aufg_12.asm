%macro ausgabe 1
	MOV DX, %1
	MOV AH, 9
	INT 21H
%endmacro

org 0x100
bits 16

SECTION .CODE
;VORHER
ausgabe vorher
MOV AL, [var]		;Transfer Inhalt nach AL
MOV DL, AL
OR  DL, 110000B		;Maskierung im hex. ASCII
	
MOV AH, 2
INT 21H

ausgabe vorschub

;NACHHER
ausgabe nachher
MOV AL, [var]		;Gleiche Zuweisung
XOR AL, AL			;Inhalt von AL wird "zeitkritisch"  a uf 0 gesetzt	
MOV [var], AL		;Neuer Inhalt in die Variable zurückspeichern
MOV DL, [var]

OR DL, 110000B		;Inhalt nach DL wegen Ausgabe
MOV AH, 2
INT 21H

;Programmende
MOV AH, 4CH
INT 21H

SECTION .DATA
var			db 6
vorher		db "Variableninhalt vorher $"
vorschub	db 10, 13, "$"
nachher		db "Variableninhalt nachher $"