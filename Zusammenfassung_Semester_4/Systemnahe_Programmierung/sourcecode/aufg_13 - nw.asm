;Irgendwas ist hier seltsam...


org 0x100
bits 16

SECTION .CODE
M1:
	mov al,7
	mov cx,8                ; 8 Bit sollen angezeigt werden
                
loop1:                                      ; Schleifen-Sprungmarke
	rcl al,1                    ; rotate left (Bits werden links aus AL-Register rotiert, unter Einbezieh-
	
	call dual                  ; ung des Carry-Flags, das dann abgefragt werden kann ==> Aufruf der
                                               ; Subroutine DUAL
                loop loop1                     ; Schleifenende: Wenn CX ungleich null, wiederhole die SChleife
                
ENDE:
                MOV AH,4CH
                int 21H
				
dual:                             ; lokale Sprungmarken
push ax                                               ; Hauptregister auf den Stack
push bx
push cx
push dx
pushf                                               ; Statuswort der Flags auf den Stack retten
dec cl                                              ; cl um 1 reduzieren, da beim nullten Bit beginnt
mov dl,cl                                          ; Zeichen nach DL
add dl,30H                                       ; Maskierung: Verwandle in den HEX-ASCII-Code
mov ah,2                                          ; FUnktion Ausgabe des Zeichens
int 21H
popf                                               ; Status-Wort (Flags) von Stack nehmen
jc eins                                            ; wenn Carry-Bit gesetzt, dann zur entsprechenden Anzeige springen
mov dx, nicht                               ; Stringausgabe 
mov ah,9
int 21H
jmp ENDEsub                                           ; wenn 0 ausgegeben wurde, dann ans Ende springen

eins:
mov dx, gesetzt                             ; Stringausgabe bei Bit = 1
mov ah,9
int 21H

ENDEsub:
	;Allgemeine Register von Stack nehmen
	pop dx
	pop cx
	pop bx
	pop ax
	ret

SECTION .DATA
gesetzt db ". Bit ist 1  ",10,13,"$"               ; Strings - für Ausgabe mit Zeilenvorschub
nicht  db ". Bit ist 0  ",10,13,"$"
