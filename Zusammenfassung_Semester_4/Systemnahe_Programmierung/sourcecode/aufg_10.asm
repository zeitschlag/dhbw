%macro ausgabe 1
	MOV DX, %1
	MOV AH, 9
	INT 21H
%endmacro

org 0x100
bits 16

SECTION .CODE
EINGABESCHLEIFE1:
	ausgabe Eingabe			;Eingabeaufforderung ausgeben
	MOV AH, 1
	INT 21H
	
	CMP AL, 30H				;Wenn ASCII kleiner 30, dann ist es keine Ziffer
		JL EINGABESCHLEIFE1	;Zurück zur Eingabe
		
	CMP AL, 39H				;Wenn ASCII größer 39, dann ist es keine Ziffer
		JG FEHLER			;Zurück zur Eingabe
		
	SUB AL, 30H
	MOV [op1], AL 
	JMP EINGABESCHLEIFE2
	
FEHLER:          
	ausgabe Fehlerm			;Fehlermeldung mit Rücksprung zu E-schleife 1
	JMP EINGABESCHLEIFE1                             
                
EINGABESCHLEIFE2:
	ausgabe Eingabe			;Eingabeaufforderung ausgeben
	MOV AH, 1				;Eingabe mit Bildschirmecho
	INT 21H
	
	CMP AL, 30H				;Wenn ASCII kleiner 30, dann ist es keine Ziffer
		JL EINGABESCHLEIFE2	;Zurück zur Eingabe
		
	CMP AL, 39H				;Wenn ASCII größer 39, dann ist es keine Ziffer
		JG FEHLER2   
		
	SUB AL, 30H				;Demaskierung
	MOV [op2], AL			;Speichern   
	JMP VERGLEICHEN
	
FEHLER2:
	ausgabe Fehlerm  
	JMP EINGABESCHLEIFE2

VERGLEICHEN:
	;Operanden stehen in 8-Bit-Registern
	MOV AL, [op1]
	MOV BL, [op2]
	
	CMP AL, BL
		JE GLEICH			;Wenn 1. OP = 2. OP gehe nach GLEICH
		JG EINSGROESSER		;Wenn der 1. > 2. dann Sprungziel EINSGROESSER	
		JMP ZEIGROESSER		;Bleibt als Restmöglichkeit übrig
	
GLEICH:
	ausgabe Meldung1
	JMP ENDE
	
EINSGROESSER:
	ausgabe Meldung2
	JMP ENDE
	
ZEIGROESSER:
	ausgabe Meldung3
	
ENDE:
	MOV AH, 4CH
	INT 21H

SECTION .DATA
Eingabe		db 10, 13, "Geben Sie eine Ziffer zwischen 0 und 9 ein!$"
Fehlerm		db 10, 13, "Das war leider keine Ziffer!$"
Meldung1	db 10, 13, "beide Zahlen sind gleich gross$"
Meldung2	db 10, 13, "die erste Zahl ist groesser$"
Meldung3	db 10, 13, "die zweite Zahl ist groesser$"

;8-Bit-Speicher-Variablen
op1	db 0
op2	db 0