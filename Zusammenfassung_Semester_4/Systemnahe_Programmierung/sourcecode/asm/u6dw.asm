ORG 100H
Bits 16

%include "asm/lib.asm"

SECTION .Code

	mov word[var_array],var1 		
	mov word[var_array+1],var2
	mov word[var_array+2],var3

	mov ax, word[var_array+2]
	xchg al, ah	
	mov cx, ax

	terminate	
 
SECTION .Data

var1 dw 0xFF00
var2 dw 0xAABB
var3 dw 0x1122

var_array: Times 3 dw 0 

%include "asm/data_lib.asm"