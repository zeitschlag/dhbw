ORG 100H
Bits 16
section .code	
               ; Ausgabe Überschrift
               mov ah,9
               mov dx,ueberschrift
               int 21H
               
               ; Ausgabe Variable kapitel1
               mov ah,9                                 ; ggf. entbehrlich
               mov dx,kapitel1
               int 21H
               
              ; Ausgabe Variable kapitel2
               mov ah,9
               mov dx,kapitel2
               int 21H
              
                MOV AH,4CH
                int 21H
		
section .data
 ueberschrift db "Assembler-Programmierung",10,13,"$"
 kapitel1     db "Kapitel 1: Registeroperationen",10,13,"$"
 kapitel2     db "Kapitel 2: Stack-Operationen",10,13,"$"
