;***************************************************************
;* Programm  : Stringausgabe  unter Verwendung des Index-Registers SI.  	       *
;*                              *
;Programmname: Oss15.ASM     *********************************************************************
;*************************************************************************************************************************
ORG 100H
Bits 16
Section  .CODE						; Code-Segment
Start:
                mov si, string1		; String-Offset nach SI
Ausgabe:
                mov dl,[si]			; Dereferenzierung von SI
                cmp dl,"$"			; War das Zeichen die Endekennung?
                je ENDE				; wenn ja, dann springe zum ENDE-Label
                mov ah,2			; Ausgabe des Zeichens
                int 21H
                inc si				; inkrementiere Index-Register
	jmp Ausgabe						; unbedingter Sprung zum Ausgabe-Label                 
ENDE:						   		; Programmende
                MOV AH,4CH
                int 21H
section .Data						; Daten-Segment
string1 db  "Ich bin ein String  $"	; Strings - f�r Ausgabe mit Zeilenvorschub
formfeed db 10,13,"$"
