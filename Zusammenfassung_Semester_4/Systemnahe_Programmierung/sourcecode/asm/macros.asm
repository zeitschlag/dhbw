%macro print 1
	push dx

	mov dx,%1
	mov ah,9
	int 21H

	pop dx
%endmacro

%macro printnum 1
	push dx

	xor dh,dh
	mov dl,byte[%1]
	add dl,30h
	mov ah,2
	int 21h

	pop dx
%endmacro

%macro printword 1
	push dx

	mov dx,[%1]
	and dx,0xFF00
	shr dx,8
	add dl,30h
	mov ah,2
	int 21h

	mov dx,[%1]
	and dx,0x00FF
	add dl,30h
	mov ah,2
	int 21h

	pop dx
%endmacro

%macro movstr 2
	; Param1: Dest, Param2: Src
	push dx
	push si
	push di

	mov si, %2
	mov di, %1

	%%copy:
		cmp byte[si],"$"
		jz %%done
		mov dl, byte[si]
		mov byte[di],dl
		inc si
		inc di
		jmp %%copy

	%%done:
		mov byte[di],"$"

		pop di
		pop si
		pop dx
%endmacro

%macro addstr 3
	; Param1: Dest, Param2: Src1, Param3: Src2
	push ax
	push dx
	push si
	push di

	mov di, %1
	mov si, %2
	call %%copy
	mov si, %3
	call %%copy
	mov al, "$"
	stosb
	jmp %%exit

	%%copy:
		cmp byte[si],"$"
		jz %%done
		mov al, byte[si]
		stosb
		inc si
		jmp %%copy

	%%done:
		ret

	%%exit:

		pop di
		pop si
		pop dx
		pop ax
%endmacro

%macro inputstr 1
	mov di, %1

	%%input:
	    mov ah,1
	    int 21h
	    cmp al,13
	    je %%done
	    stosb
	    inc bl
	    loop %%input
        
	%%done:
		; insert length check here
		mov al,"$"
		stosb
%endmacro

%macro lengthstr 2
	;Param1: out length, Param2: Src

	xor %1,%1
	mov %1,0h
	mov si, %2

	%%copy:
		cmp byte[si],"$"
		jz %%done
		inc si
		inc dh
		jmp %%copy

	%%done:

%endmacro

%macro word2str 2
	;Param1: Dest str, Param2: Src word
	push ax
	push cx
	push di
	
	mov cx,16
	mov di,%1
	push %2

	%%copy:
		pop ax
		push ax

		and ax,1000000000000000b
		shr ax,15
		add ax,30H
		stosb

		pop ax
		shl ax,1
		push ax
		loop %%copy
		jmp %%done

	%%done:
		pop ax

		pop di
		pop ax
		pop cx

%endmacro

%macro quit 0
	mov ah,4ch
	int 21h
%endmacro

