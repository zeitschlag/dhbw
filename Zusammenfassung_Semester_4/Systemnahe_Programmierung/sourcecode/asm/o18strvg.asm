;*********************************************************************************
;* Programm:  Stringvergleich 
;* Prog.Name: OSS18.ASM –                 
;*********************************************************************************
ORG 100H
Bits 16
%macro ausgabe 1					; Ausgabemakro für Texte
mov dx, %1
mov ah,9
int 21H
%endmacro

%macro laengencheck 1
mov si,%1
mov bx,si
weiter:
cmp byte[si],'$'
je rechne
inc si
jmp weiter
rechne:
mov ax,si
sub ax,bx
mov byte[q_len],al
%endmacro

section .code
laengencheck quelle
mov si,  quelle						; Offsets den Indexregistern zuweisen
mov di, ziel
mov cl, [q_len]						; Länge beider Strings nach CL
xor al,al							; AL nullen

loop1:
mov al,byte[si]
mov bl,byte[di] 					; hier mit Schleifentest
cmp al,bl                 			; berechne die Stelle der 1. Ungleichheit
jne raus
inc si
inc di
loop loop1							; Schleifenende
jmp drueber

raus:
mov al,[q_len]						; berechne Stelle der 1. Ungleichheit
sub al,cl
mov byte[stelle],al					; sichere diese Stelle
inc byte[stelle]					; inkrementiere diese, da String beim 
ausgabe formfeed					; nullten Zeichen anfängt
ausgabe meldung2					; Ausgaben im Fall der Ungleichheit

ausgabe  meldung3
mov al,[stelle]						; Vorbereitung der Ausgabe für die
add al,30H							; berechnete Stelle
mov dl,al							; Ziffer am Bildschirm ausgeben
mov ah,2
int 21H
jmp Ende 							; springe zum ENDE-Label

drueber:
ausgabe meldung1					; wenn Strings identisch, dann Ausgabe
Ende:
        mov ah, 4cH
        Int 21H
Section .DATA
stelle db 0	
meldung1 db "Beide Strings sind identisch! ","$"	; Ausgabetexte
meldung2 db "Die beiden Strings sind unterschiedlich! ","$"
meldung3 db "Unterschied ab Zeichen: $"

formfeed db 10,13,"$"
blank db " $"
											; Variable zum Fixieren des Vor-
quelle db "ein vordefinierter String$"		; kommens des 1. Unterschiedes
ziel db   "ein vordefinierter String$"		; Quell- und Zielstrings deklarieren
q_len db 0
