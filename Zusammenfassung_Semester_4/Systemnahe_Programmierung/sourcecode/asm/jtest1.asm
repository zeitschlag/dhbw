org 100H
bits 16

%include "asm/macros.asm"

section .CODE
	print prompt
	jmp input

	input:
		mov ah,8
		int 21h
		cmp al,13
		je end
		mov [inputstring],al
		print inputstring
		sub ax,30H
		xor ah,ah
		shl ax,4
		push ax
		mov di,outputstring
		mov cx,4
		jmp outputcharacter

	outputcharacter:
		pop ax
		push ax

		and al,10000000b
		shr al,7
		add al,30H
		stosb

		pop ax
		shl ax,1
		push ax
		loop outputcharacter
		jmp outputend

	outputend:
		print outputstring
		jmp input

	end:
		quit

section .data

	prompt db "Geben Sie eine Zahl ein: ",10,13,"$"
	inputstring db ".",10,13,"$"
	outputstring db "....",10,13,"$"
