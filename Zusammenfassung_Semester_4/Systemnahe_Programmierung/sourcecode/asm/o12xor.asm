;*****************************************************************************************************
;* Programm : Variableninhalt auf 0 setzen und rückspeichern        *
; Programmname: Oss12.ASM      ***********************************************************************
;*************************************************************************************************************************


ORG 100H    
Bits 16

%macro ausgabe 1
mov dx,%1
mov ah,9
int 21H
%endm

section .CODE                   			; Code-Segment
                                 
Start:
                ausgabe vorher
                
                mov al,[var]        		; Transfer Inhalt nach AL
                mov dl,al              		; dito nach DL
                or  dl, 110000B
											; Maskierung im hex. ASCII	
                 mov ah,2                	; Bildschirmausgabe
                 int 21H
                ausgabe vorschub
                ausgabe nachher
                mov al,[var]           		; gleiche Zuweisung
                xor al,al                   ; Inhalt von AL wird "zeitkritisch"  a uf 0 gesetzt	
                mov [var],al             	; neuer Inhalt in die Variable zurückspeichern
                mov dl,[var]
                  
                or  dl, 110000B        		; Inhalt nach DL wegen Ausgabe
											; s.o.
                mov ah,2
                int 21H  

                
				;Programmende
                MOV AH,4CH
                int 21H
		

section .Data                           	; Daten-Segment

var db 6
vorher db "Variableninhalt vorher $"
vorschub db 10,13,"$"
nachher db "Variableninhalt nachher $"
