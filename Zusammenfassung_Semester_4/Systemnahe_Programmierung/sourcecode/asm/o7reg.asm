;***************************************************************
;* Programm Oss7.asm: Ausgabe AX-Register              *
;*	                        nur code-Section                      *
;***************************************************************
ORG 100H
Bits 16
section .code				; Beginn Code-Segment
                
MOV AX, 3578H			    ; AX mit Anfangswert laden
PUSH AX	                                           ; 
                                                                                 

                            ; vertausche AH und AL
XCHG  AH,AL					; nulle AH-Register	
XOR  AH,AH
PUSH AX			         	; AX auf Stack legen
AND AL,11110000B  			; maskiere AL
MOV Cl,4
SHR al,cl	                ; in AL 4 Bits nach rechts (identisch mit Division /10hex ;Zeichenausgabe �ber DL-Register	
ADD AL,30H       
MOV DL,AL
MOV AH,2
INT 21H

POP AX						; letzten gespeicherten Wert von AX von Stapel
AND AL,00001111B			; maskiere AL

;Zeichenausgabe über DL-Register; 
ADD AL,30H   
MOV DL,AL
MOV AH,2
INT 21H

POP AX						; vorletzten gepeicherten Wert vom Stack holen
XOR AH,AH 					; nulle AH
PUSH AX				   		; lege aktuellen AX-Inhalt auf Stack	
AND AL,11110000B   			; maskiere AL			
MOV CL,4					; lade Anzahl der zu schiebenden Zeichen			   ;	
SHR AL,CL 					; schiebe Anzahl von cl Zeichen in AL nach rechts

;Zeichenausgabe über DL-Register; 
ADD AL,30H
MOV DL,AL
MOV AH,2
INT 21H

POP AX						; letzten gesicherten AX-Inhalt vom Stapel
AND AL,00001111B	        ; maskiere AL

;Zeichenausgabe über DL-Register; 
ADD AL,30H
MOV DL,AL
MOV AH,2
INT 21H

  
MOV AH,4CH
int 21H
                