
; Programm  : Stringeingabe  unter Verwendung der Funktion 02H mit STOSB                  
; Programmname: Oss16.ASM     


%macro ausgabe 1			;ausgabe macro einfache Stringausgabe
mov dx, %1
mov ah,9
int 21H
%endmacro

ORG 100H
Bits 16
Section .CODE				; Beginn Code-Segment
Start:
							; initialisiere Daten- und Extra-Segment
        
                            ; hier ist das Extra-Segment wirklich notwendig!
ausgabe meldung1
mov di, estring1			; für STOSB muss das Zielindex-Register verwendet werden!
Eingabe:
 mov ah,01H					; Zeicheneingabe mit Bildschirmecho
 int 21H
 stosb						; speichert das in AL stehende Zeichen an die Stelle [DI]
 cmp al,13					; wenn Enter nicht gedrückt wurde
 jne Eingabe				; bedinter Sprung zum Eingabe-Label
 mov al,"$"					; letzte Eingabe (Enter) durch Ende-Kennung überschrieben
 stosb						; wiederum durch STOSB-Befehl
fertig:
 ausgabe formfeed			; jetzt kann der String durch die Funktion 09H
ausgabe estring1			; am Bildschirm ausgegeben werden!
ende:						; Programmende

        mov ah, 4cH
        Int 21H
		
Section .DATA
meldung1 db "Erfassen eines Strings ueber Tastatur",10,13,"$"
formfeed db 10,13,"$"
estring1 db  "....................$"