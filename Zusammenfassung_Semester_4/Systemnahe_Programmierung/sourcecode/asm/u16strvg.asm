;*********************************************************************************
;* Programm:  Stringvergleich 
;* Prog.Name: OSS18.ASM –                 
;*********************************************************************************
ORG 100H
Bits 16

%include "asm/lib.asm"

section .code

str_len quelle, q_len
str_len ziel, z_len

mov al, [q_len]
mov bl, [z_len]
cmp al, bl
jg len_g
		; Zielstring länger
		xor cx,cx	
		mov cl, [z_len]
		mov [len], cl
		jmp endlen

	len_g:
		xor cx,cx	
		mov cl, [q_len]
		mov [len], cl
endlen:

mov si,  quelle						; Offsets den Indexregistern zuweisen
mov di, ziel			
xor al,al
xor bl,bl
xor cl,cl

mov cl, [len]						

loopVergleich:
	mov al,byte[si]
	mov bl,byte[di] 					; hier mit Schleifentest
	cmp al,bl                 			; berechne die Stelle der 1. Ungleichheit
	jne ungleich
	inc si
	inc di
	loop loopVergleich							; Schleifenende
jmp gleich

ungleich:
	mov al,[len]						; berechne Stelle der 1. Ungleichheit
	sub al,cl
	mov byte[stelle],al					; sichere diese Stelle
	inc byte[stelle]					; inkrementiere diese, da String beim 

	strout newline						; nullten Zeichen anfängt
	stroutln stringUnter					; Ausgaben im Fall der Ungleichheit
	strout unterschied

	mov al,[stelle]						; Vorbereitung der Ausgabe für die
	toAscii al,al
	byteout al
	jmp Ende 							; springe zum ENDE-Label

gleich:
	strout stringGleich
						
Ende:
	strout newline
	strout newline	
	stroutln quelle	
	stroutln ziel
	terminate

Section .DATA

stringGleich db "Beide Strings sind identisch! ","$"	; Ausgabetexte
stringUnter db "Die beiden Strings sind unterschiedlich! ","$"
unterschied db "Unterschied an Zeichen: $"

newline db 10,13,"$"
blank db " $"
											; Variable zum Fixieren des Vor-
quelle db "123$"		; kommens des 1. Unterschiedes
ziel db   "1234$"		; Quell- und Zielstrings deklarieren
q_len db 0
z_len db 0
len db 0
stelle db 0	
