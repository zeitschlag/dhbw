org 0x100
bits 16

%macro pushall 0
	push ax
	push bx
	push cx
	push dx
%endmacro

%macro popall 0
	pop dx
	pop cx
	pop bx
	pop ax
%endmacro

%macro print_low_nibble 1
	pushall

	; empty high nibble
	and %1, 0fh

	; if %1 > 9 
		cmp %1, 9
		jg %%hex

		; then
			add %1, 30h
			jmp %%endif

		; else
			%%hex:
			add %1, 37h

	;endif
		%%endif:

	; do the actual printing
	mov dl, %1
	mov ah, 2
	int 21h

	popall
%endmacro

%macro print 1
	mov ah,9
	mov dx,%1
	int 21h
%endmacro

section .code

	mov ax, cs
	mov ds, ax

	mov di, string

	; loop
		getchar:

		; if length == 15
			cmp byte[length], 15
			je endgetchar
		
		; get char
			mov ah, 0x01
			int 0x21

		; if gotten character == newline
			cmp al, 13
			je endgetchar

		stosb



		inc byte[length]

		jmp getchar

	; endloop
		endgetchar:

	mov al, "$"
	stosb

	print newline

	print_low_nibble byte[length]

	print newline
	print newline

	; printstr
	print string

	; exit
		mov ah, 0x4C
		int 0x21

section .data
	newline db 10,13,"$"
	length db 0
	string db "..............$"