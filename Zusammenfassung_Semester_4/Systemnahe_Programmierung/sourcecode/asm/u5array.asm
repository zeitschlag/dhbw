;***************************************************************
;* Verwendung von Variablen *
;* Prog.Name: oss5.ASM    *
;***************************************************************

ORG 100H
Bits 16
SECTION .CODE

%include "asm/lib.asm"

Start:						; Programm-Anfangs-Label
	;MOV ax,CS
	;MOV DS,ax				; Initialisiere Datensegment per
	mov bl, 'A' 			; Buchstabe A nach bl

	mov [array1],BL 		; speichere an Adresse array1
	inc bl 					; erhöhe BL (Buchstabe B)
	mov [array1+1],bl 		; speichere an Adresse array1+1
	inc bl 					; erhöhe BL (Buchstabe C)
	mov [array1+2],bl 		; speichere an Adresse array1+2
	inc bl
	mov [array1+3],bl 

mov cx, 4
mov dx, 0
strout start_a

loopstart:
	mov bx, array1
	add bx, dx
	mov al, [bx]
	zout al

	inc dx
	cmp cx, 1
	je loopend
	strout comma

	loopend:
loop loopstart

strout end_a

terminate

SECTION .Data

index db 0
array1: Times 5 db 0 		; Byte-Array mit 0 initialisiert
comma db ",$"
start_a db "[$"
end_a db "]$"

%include "asm/data_lib.asm"