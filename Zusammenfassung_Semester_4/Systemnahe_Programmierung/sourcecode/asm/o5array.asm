;***************************************************************
;* Verwendung von Variablen *
;* Prog.Name: oss5.ASM    *
;***************************************************************

ORG 100H
Bits 16
SECTION .CODE

Start:						; Programm-Anfangs-Label
	MOV ax,CS
	MOV DS,ax				; Initialisiere Datensegment per
	mov bl, 'A' 			; Buchstabe A nach bl

	mov [array1],BL 		; speichere an Adresse array1
	inc bl 					; erhöhe BL (Buchstabe B)
	mov [array1+1],bl 		; speichere an Adresse array1+1
	inc bl 					; erhöhe BL (Buchstabe C)
	mov [array1+2],bl 		; speichere an Adresse array1+2
	inc bl
	mov [array1+3],bl 
	MOV AH, 4CH 			; Programmende
	int 21H


SECTION .Data

array1: Times 5 db 0 		; Byte-Array mit 0 initialisiert