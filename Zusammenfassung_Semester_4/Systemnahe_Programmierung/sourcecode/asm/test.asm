ORG 100H
Bits 16

%include "asm/lib.asm"

SECTION .Data
	cleanreg

        mov byte [buffer+9],'$'
        lea si,[buffer+9]

        MOV CX, 0x00FF

        MOV AX,CX         ;CX = VALUE THAT I WANT TO CONVERT
        MOV BX,10         
ASC2:
        mov dx,0            ; clear dx prior to dividing dx:ax by bx
        DIV BX              ;DIV AX/10
        ADD DX,48           ;ADD 48 TO REMAINDER TO GET ASCII CHARACTER OF NUMBER 
        dec si              ; store characters in reverse order
        mov [si],dl
        CMP AX,0            
        JZ EXTT             ;IF AX=0, END OF THE PROCEDURE
        JMP ASC2            ;ELSE REPEAT
EXTT:
        mov ah,9            ; print string
        mov dx,si
        int 21h

	terminate	
 
SECTION .Data

%include "asm/data_lib.asm"

buffer: Times 10 db 0 
