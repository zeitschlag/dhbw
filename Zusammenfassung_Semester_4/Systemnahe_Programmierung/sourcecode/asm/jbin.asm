org 100H
bits 16

%include "asm/macros.asm"

section .Code

	xor ax,ax

	;lahf ; Push die Status-Flags auf AH

	pushf ; Pusht alle Flags
	pop ax

	mov ax,0xFFFF

	word2str outputstring,ax
	
	print outputstring

	quit

section .DATA

	outputstring db "................",10,13,"$"
