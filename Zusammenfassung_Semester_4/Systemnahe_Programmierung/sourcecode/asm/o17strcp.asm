;* Programm : Stringcopy unter Verwendung der beiden indexregister SI und DI *

  						; Konstante für CR-Tasten-ASCII-Code

%macro ausgabe 1  ; Macro für Textausgabe
	mov dx, %1
	mov ah,9
	int 21H
%endmacro
ORG 100H


Section .code  ; Beginn Codesegment
ORG 100H

ausgabe formfeed  ; Bildschirmausgaben 
ausgabe meldung1 
ausgabe quelle
ausgabe formfeed

mov si, quelle  		; Referenz des Quellstrings nach SI
mov di, ziel 			; Referenz des Zielstrings nach DI
copy: 					; Label für Schleifenanfang
	cmp byte[si],"$" 	; war das adressierte Zeichen ein $ ?
	jz raus   
	mov dl, byte[si] 	; kopiertes Zeichen in 8-Bit-Register
	 
	mov byte[di],dl 	; von dort aus an Byte-Adresse im Zielstring
	inc si  			; erhöhe Source-Index
	inc di  			; erhöhe Destination-Index
	jmp copy
raus:
	mov byte[di],"$" 	 ; hänge Endekennung an
	 				 	; nach Ziel. Durch den rep-Befehl werden alle
	  					; Zeichen kopiert
	ausgabe formfeed   ; Bildschirmausgaben
	ausgabe meldung2 
	ausgabe ziel
ENDE:   ; Programmende
	mov ah, 4cH
	Int 21H

Section .DATA
	meldung1 db "Ausgabe des Quellstrings: ","$" ; Meldungen 
	meldung2 db "Ausgabe des kopierten Zielstrings: ","$"
	formfeed db 10,13,"$"  ; Deklaration formfeed/linefeed
	quelle db "Das ist ein vordefinierter String$" ; Quellstring (initialisiert)
	ziel db "**********************************************************************$" 
; Zielstring: als Bereich mit 40 * 