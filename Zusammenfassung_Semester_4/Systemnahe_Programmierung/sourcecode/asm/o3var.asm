;***************************************************************
;* Verwendung von Variablen  - Offset nach CX            *
;* Prog.Name: oss3.ASM 		             	       *
;***************************************************************
ORG 100H
Bits 16
SECTION .CODE			
Start:                                	; Programm-Anfangs-Label
         MOV ax,CS                  	; Initialisiere Datensegment per
	     MOV DS,ax               
         MOV cx, variable          		; lade Offset der Variablen-Adresse 
                                                   
         MOV AH, 4CH                    ; Programmende
         int 21H
		

SECTION .Data
variable dw 32ABH                       ; Definition einer WORT-Variablen
