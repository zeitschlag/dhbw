ORG 100H
Bits 16

%include "asm/lib.asm"

SECTION .Data

pushf
pop AX

outRegister AX

strout newline
strout newline

mov cx, 0x10
startloop:                                    
    rcl AX, 1  

    pushall
    pushf 

    dec cl                        
    toHex cl, cl
    byteout cl

    popf
    printCF

popall          
loop startloop

terminate
 
SECTION .Data

%include "asm/data_lib.asm"