;***************************************************************
;* Programm  : Stringausgabe  unter Verwendung des LODSB-Befehls  	       *
;*                              *
;Programmname: Oss15a.ASM     *********************************************************************
;*************************************************************************************************************************
ORG 100H
Bits 16

section .CODE                   	; Code-Segment
ORG 100H             				; lege Code ab Speicherstelle 100 Hex ab
Start:
                
	mov si, string1					; String-Offset nach SI
Ausgabe:
                LODSB				; LODS-Befehl lädt Zeichen in AL-Register
				Mov dl,al			; Kopieren nach DL zur Ausgabe
				cmp dl,'$'			; war das Zeichen die Endekennung?
                Je ENDE				; wenn ja, dann springe ans Ende
                mov ah,2			; Zeichenausgabe
                int 21H
                jmp Ausgabe                 
ENDE:						   		; Programmende
                MOV AH,4CH
                int 21H
section  .data                      		; Daten-Segment
string1 db    "Ich bin ein String  $"       ; Strings - für Ausgabe mit Zeilenvorschub
formfeed db 10,13,"$"
