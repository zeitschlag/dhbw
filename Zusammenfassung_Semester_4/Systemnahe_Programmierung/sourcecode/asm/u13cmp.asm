ORG 100H
Bits 16

%include "asm/lib.asm"

%macro readInput 0
    mov ah,1
    int 21H

    cmp al,30H                                          ; wenn ASCII kleiner 30, dann ist es keine Ziffer
    jl Fehler1Ziffer1                                   ; zurück zur Eingabe

    cmp al,39H                                          ; wenn ASCII größer 39, dann ist es keine Ziffer
    jg Fehler1Ziffer1  
%endmacro

SECTION .Data

Eingabeschleife1Ziffer1:        
                strout Eingabe1                                 ; Eingabeaufforderung ausgeben

                readInput

                mov [op11],al
                jmp Eingabeschleife1Ziffer2

Fehler1Ziffer1:          
                strout Fehler                                    ; Fehlermeldung mit Rücksprung zur Eingabe der 1. Ziffer von Zahl 1
                jmp Eingabeschleife1Ziffer1      

Eingabeschleife1Ziffer2:

				readInput

                sub al,30H
                mov [op12],al 
                jmp Eingabeschleife2Ziffer1

Fehler1Ziffer2:          
                strout Fehler                                    ; Fehlermeldung mit Rücksprung zur Eingabe der 2. Ziffer von Zahl 1
                jmp Eingabeschleife1Ziffer2                            
                                 


                                                                    



Eingabeschleife2Ziffer1:                                            ; Eingabe der 2. Zahl
                strout Eingabe1                                  ; Eingabeaufforderung ausgeben

				readInput

                mov [op21],al
                jmp Eingabeschleife2Ziffer2

Fehler2Ziffer1:          
                strout Fehler                                    ; Fehlermeldung mit Rücksprung zur Eingabe der 1. Ziffer von Zahl 2
                jmp Eingabeschleife2Ziffer1      

Eingabeschleife2Ziffer2:

				readInput

                sub al,30H
                mov [op22],al
                jmp Vergleich

Fehler2Ziffer2:          
                strout Fehler                                    ; Fehlermeldung mit Rücksprung zur Eingabe der 2. Ziffer von Zahl 2
                jmp Eingabeschleife2Ziffer2  






Vergleich:                                                          ; Compare me maybe
                mov al,[op11]
                mov bl,[op21]
                cmp al,bl                                           ; Vergleiche die ersten Ziffern
                je Gleich1                                          ; wenn 1. OP = 2. OP -> 2. Überprüfung
                jg Groesser1                                        ; wenn der 1. > 2. ist Zahl 1 groesser 
                jmp Groesser2                                       ; sonst ist Zahl 2 groesser
               
Gleich1:
                mov al,[op12]                                       
                mov bl,[op22]
                cmp al,bl                                           ; Vergleiche die zweiten Ziffern
                je Gleich2                                          ; wenn 1. OP = 2. OP sind beide Zahlen gleich
                jg Groesser1                                        ; wenn der 1. > 2. ist Zahl 1 groesser  
                jmp Groesser2                                       ; sonst ist Zahl 2 groesser

Gleich2:
                strout MGleich
                jmp Ende                                            ; nach Ausgabe ans Programmende springen
                         
Groesser1:
                strout MGroesser1
                jmp Ende                                            ; nach Ausgabe zum Programmende       

Groesser2:
                strout MGroesser2
                jmp Ende       

Ende:
				terminate	
 
SECTION .Data

        Eingabe1    db 10,13,"Geben Sie eine Zahl zwischen 0 und 99 ein!$"
        Eingabe2    db 10,13,"Geben Sie eine weitere Zahl zwischen 0 und 99 ein!$"
        Fehler      db 10,13,"Das war leider keine Ziffer! $"

        MGleich     db 10,13,"Beide Zahlen sind gleich gross$"
        MGroesser1  db 10,13,"Die erste Zahl ist groesser$"
        MGroesser2  db 10,13,"Die zweite Zahl ist groesser$"

        op11        db 0                                                     ; 8-Bit-Speicher-Variablen
        op12        db 0
        op21        db 0
        op22        db 0


%include "asm/data_lib.asm"