;**************************************************************
;* Programm OSS11.ASM: Schleifenkonstrukt mit LOOP
;***************************************************************


Org 100H
Bits 16
Section  .CODE
Start:
                MOV cx,5
                MOV dx,40H
    marke:
                inc dx
                MOV ah,2
                Int 21H
    loop marke

ENDE:
                MOV AH,4CH
                int 21H
