org 100H
bits 16

%include "asm/macros.asm"

section .CODE

	print prompt

	inputstr eingabe

	print meldung1
	print eingabe
	print newline

	movstr ziel, eingabe

	print meldung2
	print ziel
	print newline

	addstr ziel2, ziel, teil2

	print newline
	print ziel2

	lengthstr dx, eingabe

	xchg dh,dl

	mov [laenge], dx

	printnum laenge

	quit

section .DATA

	prompt 		db "Bitte String eingeben:",10,13,"$"
	eingabe		db "**********************************************************************$"

	meldung1 	db "Quellstring: ","$"
	meldung2 	db "Kopierter Zielstring: ","$"
	newline 	db 10,13,"$"
	ziel 		db "**********************************************************************$"

	teil2 		db " #yolo$"
	ziel2		db "**********************************************************************$"

	laenge 		db 0h
