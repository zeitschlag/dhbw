%macro pushall 0
    push ax
    push bx
    push cx
    push dx
%endmacro

%macro popall 0
    pop dx
    pop cx
    pop bx
    pop ax
%endmacro

%macro cleanreg 0
    xor ax,ax
    xor bx,bx
    xor cx,cx
    xor dx,dx
%endmacro

%macro strout 1 ; Param1: string
pushall
    mov ah,9
    mov dx,%1
    int 0x21
popall
%endmacro

%macro bytein 0 
    mov ah, 01h
    int 21h
%endmacro

%macro strin 1 ; Param1: string
pushall
mov di, %1

    ; loop
        getchar:
        
        ; get char
            mov ah, 0x01
            int 0x21

        ; if gotten character == newline
            cmp al, 13
            je endgetchar

        stosb
        jmp getchar

    ; endloop
        endgetchar:

    mov al, "$"
    stosb
popall
%endmacro

%macro byteout 1
pushall
    mov ah,2
    mov dl,%1
    int 0x21
popall
%endmacro

%macro stroutln 1 ; Param1: string
pushall
    strout %1
    strout newline
popall
%endmacro

%macro terminate 0
    MOV AH,4CH
    int 21H
%endmacro

%macro toHex 2 ; Param1: dest reg, Param2: src reg )
    mov %1,  %2
    cmp %1,9 
    jg %%hex
    add %1,0x30     ;Addieren des Hex offset für Ziffern
    jmp %%ifende       ;Unbedingter Sprung zur Ausgabe
    %%hex:  add %1,0x37     ;Addieren des Hex offset für Buchstaben
    %%ifende:
%endmacro

%macro toAscii 2 ; (dest, src)
    mov %1,  %2
    add %1,0x30     
%endmacro

%macro outLOWregister 1 ; Param1: reg
pushall
    mov AX, %1
    push AX             ;AX Auf den Stapel legen
    and AX,0x00FF
    mov cl,4            ;Anzahl Bit die geschoben werden soll
    shr AX,cl           ;AL um CL nach rechts schieben (zweite Ziffer eliminieren)
    toHex al, al
    byteout al

    pop AX              ;restore AX from stack
    and al,0x0F         ;Remove first sign
    toHex al, al
    byteout al
popall
%endmacro

%macro outHIGHregister 1 ; Param1: reg
pushall
    mov cl,8
    shr %1,cl
    outLOWregister %1 
popall 
%endmacro

%macro outRegister 1 ; Param1: reg
pushall
    outHIGHregister %1
    outLOWregister %1
popall   
%endmacro

%macro printCF 0
    jc %%eins                                    
        strout zero
        jmp %%ifende                               
    %%eins:
        strout one

    %%ifende:                                     
%endmacro

%macro str_len 2 ; Param1: string, Param1: byte length
pushall
    mov si,%1
    mov bx,si
    %%weiter:
        cmp byte[si],'$'
        je %%rechne
        inc si
        jmp %%weiter
    %%rechne:
        mov ax,si
        sub ax,bx
        mov byte[%2],al
popall
%endmacro