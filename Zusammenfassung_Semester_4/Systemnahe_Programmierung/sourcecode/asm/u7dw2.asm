ORG 100H
Bits 16

%include "asm/lib.asm"

SECTION .Code

	mov al, byte[var1]
	mov bl, byte[var2]
	mov byte[var1], bl
	mov byte[var2], al

	mov al, byte[var1+1]
	mov bl, byte[var2+1]
	mov byte[var1+1], bl
	mov byte[var2+1], al

	mov ax, word[var1]
	mov bx, word[var2]

	terminate	
 
SECTION .Data

var1 dw 0xAABB
var2 dw 0x1122

%include "asm/data_lib.asm"