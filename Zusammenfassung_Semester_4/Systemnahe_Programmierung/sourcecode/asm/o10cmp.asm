;*****************************************************************************************************
;* Programm       : Eingabe zweier einstelliger Ziffern mit G�ltigkeitsabfragen und Gr��envergleich       ;*
;Programmname: Oss10.ASM      ***********************************************************************
;*************************************************************************************************************************
;		                                             *
;*****************************************************************************************************
%macro ausgabe 1             ; Macro f�r Stringausgaben
mov dx,%1			         ; Parameter: Anzahl Param	
mov ah,9
int 21H
%endm


Org 100H
Bits 16
section .CODE								
ORG 100H
Start:
        

Eingabeschleife1:		 						; bei Falscheingaben Sprungziel	
                ausgabe Eingabe                 ; Eingabeaufforderung ausgeben
                mov ah,1
                int 21H
                cmp al,30H		  				; wenn ASCII kleiner 30, dann ist es keine Ziffer
                jl Fehler		  				; zur�ck zur Eingabe
                cmp al,46H		  				; wenn ASCII gr��er 39, dann ist es keine Ziffer
                jg Fehler          		  		; zur�ck zur Eingabe
                sub al,30H
                mov [op1],al 
                jmp Eingabeschleife2
Fehler:          
                ausgabe Fehlerm 				; Fehlermeldung mit R�cksprung zu E-schleife 1
                jmp Eingabeschleife1                             
                
Eingabeschleife2:	                 			; bei Falscheingaben Sprungziel	  
                ausgabe Eingabe          		; Eingabeaufforderung ausgeben
                mov ah,1						; Eingabe mit Bildschirmecho
                int 21H
                cmp al,30H		 				; wenn ASCII kleiner 30, dann ist es keine Ziffer
                jl Eingabeschleife2		 		; zur�ck zur Eingabe
                cmp al,46H		 				; wenn ASCII gr��er 39, dann ist es keine Ziffer
                jg Fehler2   
                sub al,30H     		 			; Demaskierung
                mov [op2],al                    ; speichern   
                jmp Vergleichen
Fehler2:                                        ; Fehlermeldung mit R�cksprung zu E-schleife 2
                ausgabe Fehlerm  
                jmp Eingabeschleife2

Vergleichen:                                   	; vergleichen
                mov al,[op1]
                mov bl,[op2]		   			;Operanden stehen in 8-Bit-Registern
                cmp al,bl
                je gleich		   				; wenn 1. OP = 2. OP gehe nach GLEICH
                jg einsgroesser		   			; wenn der 1. > 2. dann Sprungziel EINSGROESSER	
                jmp zweigroesser		   		; bleibt als Restm�glichkeit �brig
               

gleich:                                         ; Sprungziele mit entsprechenden Stringausgaben
                ausgabe Meldung1
                jmp ende						; nach Ausgabe ans Programmende springen

                         
einsgroesser:
                ausgabe Meldung2
                jmp ende	                   	; nach Ausgabe zum Programmende		
zweigroesser:
                 ausgabe Meldung3
                                                         
            

 
ende:								   			;Programmende
                MOV AH,4CH
                int 21H
		
section .Data
Eingabe  db 10,13,"Geben Sie eine Ziffer zwischen 0 und 9, oder A und F ein!$"   ; Meldungen f. Bildschirm
Fehlerm   db 10,13,"Das war leider keine Ziffer! $"
Meldung1 db 10,13,"beide Zahlen sind gleich gross$"
Meldung2 db 10,13,"die erste Zahl ist groesser$"
Meldung3 db 10,13,"die zweite Zahl ist groesser$"

op1      db 0                                                     ; 8-Bit-Speicher-Variablen
op2      db 0
