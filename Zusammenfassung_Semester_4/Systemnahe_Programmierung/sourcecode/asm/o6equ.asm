;*****************************************************************************
;* Die EQU - Pseudoansweisung ==> Durchführung einer einfachen *
;* Prog.Name: oss6.ASM 	             Divisions-Rechnung                 *
;*****************************************************************************

rech_betrag EQU 12				; Konstanten mit Entsprechungen
mwst EQU 19	                	; funktioniert mit numerischen Inhalten
teiler EQU 100
ORG 100H
Bits 16
section .CODE	       	    
	mov BX, mwst                  ; Weise die Konstante mwst zu
	mov ax, rech_betrag           ; lade AX mit Konstanten rech_betrag
	mul BX                        ; multipliziere AX mit BX, Ergebnis nach AX
	mov cx, teiler                ; Konstante teiler wird cx zugewiesen
	div cx                        ; Ergebnis (Vorkommastelle) steht in
								  ; AX, Nachkommastelle in DX
	;Beenden
	mov ah, 4cH                   ; Zuweisung des Beendigungscodes Mov ah,4Ch
	int 21H                       ; Konstante für Interrupt 21H	
