ORG 100H
Bits 16

%include "asm/lib.asm"

SECTION .Data
	cleanreg

	mov bl, 0
	mov cl, 10

	loopjmp:

			push bx
			shr bx, 1
			pop bx

			pushf
			cmp bx, 0
			je continue
			popf

			jc ungerade
				toAscii al, bl
				byteout al
				strout comma
				jmp continue
			ungerade:

				jmp continue

			continue:

			inc bx

	loop loopjmp

	terminate	
 
SECTION .Data

%include "asm/data_lib.asm"

comma db ", $"
str_ungerade db " Zahl ungerade $"
str_gerade db " Zahl ungerade $"