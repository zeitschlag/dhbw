ORG 100H
Bits 16

%include "asm/lib.asm"

SECTION .Data
	cleanreg

	bytein
    sub al,48
	mov dl, al
	imul dx, 1000
	add bx, dx

	or dx,dx
	bytein
    sub al,48
	mov dl, al
	imul dx, 100
	add bx, dx

	or dx,dx
	bytein
    sub al,48
	mov dl, al
	imul dx, 10
	add bx, dx

	or dx,dx
	bytein
    sub al,48
	mov dl, al
	imul dx, 1
	add bx, dx

	mov ax, bx

	strout newline

	outRegister ax

	terminate	
 
SECTION .Data

%include "asm/data_lib.asm"