org 0x100
bits 16

SECTION .CODE
M1:
	MOV AX, CS				;Initialisiere Datensegment per Ringtausch
	MOV DS, AX
	
	MOV BL, 'A'				;Buchstabe A nach bl
	MOV [array1], BL		;Speichere an Adresse array1
	
	INC BL					;Erhöhe BL (Buchstabe B)
	MOV [array1+1], BL		;Speichere an Adresse array1+1

	INC BL					;Erhöhe BL (Buchstabe C)
	MOV [array1+2], BL		;Speichere an Adresse array1+2

	INC BL					;Erhöhe BL (Buchstabe D)
	MOV [array1+3], BL		;Speichere an Adresse array1+3
	
	MOV AH, 4CH				;Programmende
	INT 21H

SECTION .DATA
array1: Times 5 db 0		;Byte-Array mit 0 initialisiert
