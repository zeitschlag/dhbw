org 0x100
bits 16

SECTION .CODE
M1:
	XOR AX, AX				;Setze AX und BX auf 0
	XOR BX, BX				;Kürzer als MOV BX,0
	MOV AL, [op1]			;Inhalt Operand 1 nach AL
	MOV BL, [op2]			;Inhalt Operand 2 nach BL
	
	CMP AL, BL
		JA ERSTERGROESSER	;Wenn erster größer dann Sprung
		JMP ZWEITERGROESSER

ERSTERGROESSER:
	MOV DX, Meldung1		;Ausgabefunktion Zeichenketten
	MOV AH, 9
	INT 21H
                
WEITER:
	XOR AX, AX				;Setze AX und BX auf 0
	XOR BX, BX
	MOV AL, [op3]			;Inhalt Operand 3 nach AL
	
	MOV BL, [op4]			;Operand 4 nach BL

	CMP AL, BL
		JL  ZWEITERGROESSER	;Wenn 2. kleiner
		JMP WEITER1

ZWEITERGROESSER:
	MOV DX, Meldung2
	MOV AH, 9
	INT 21H
                
WEITER1:         
	XOR AX, AX				;Anweisungen für gleich grosse
	XOR BX, BX
	MOV AX, [op5]
	MOV BX, [op6]
	CMP AX, BX
		JE GLEICHGROSS
		JMP ENDE

GLEICHGROSS:
	MOV DX, Meldung3
	MOV AH, 9
	INT 21H

ENDE:
	MOV AH, 4CH
	INT 21H

SECTION .DATA
Meldung1	db 10, 13, "erster Operand ist groesser$"
Meldung2	db 10, 13, "zweiter Operand ist groesser$"
Meldung3	db 10, 13, "beide sind gleich gross$"

;Operanden
op1	db 5
op2	db 3
op3	db "A"		;Hier mit unterschiedl. ASCII	
op4	db "X"

;WORD-Variablen
op5	dw 1234H
op6	dw	1234H

