org 0x100		;Beginne Programm bei Adresse 100H
bits 16			;DOS läuft im real-mode -> 16bit

SECTION .CODE
M1:
	MOV AH, 08H	;liest Zeichen von Tastatur ohne Bildschirmecho
	INT 21H
	CMP AL, 1BH	;vergleiche Inhalt von AL mit ESC-Taste
	JE ENDE		;wenn gleich, dann springe zu ENDE-LABEL
	MOV dl, al	;bringe Eingabe-Zeichen ins rechte Daten-Byte
	MOV ah, 2	;Funktionsnummer für Bildschirmausgabe
	INT 21H
	JMP M1		;unbedingter Sprung zum M1-Label
ENDE:
	MOV AH, 4CH	;Funktionsnummer Programm beenden
	Int 21H
