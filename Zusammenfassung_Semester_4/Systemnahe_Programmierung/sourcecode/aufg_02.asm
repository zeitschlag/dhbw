org 0x100
bits 16

SECTION .CODE
M1:
	MOV AX, CS				;Initialisiere Datensegment per Ringtausch
	MOV DS, AX
	MOV BX, 15A3H			;Lade bx mit beliebigem Hexwert
	MOV word[variable], BX	;Inhalt von BX der Variablen variable zuweisen
	MOV AH, 4CH				;Programmende
	INT 21H

SECTION .DATA
variable dw 0				;Definition einer WORT-Variablen
