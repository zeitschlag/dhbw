org 0x100
bits 16

SECTION .CODE
M1:
	MOV AX, CS				;Initialisiere Datensegment per Ringtausch
	MOV DS, AX
	MOV CX, variable		;Lade Offset der Variablen Adresse
	MOV BL, byte[variable]	;Lade niedrigwertigen 8-Bit-Inhalt
	MOV BH, byte[variable+1];Lade höherwertigen 8-Bit-Inhalt
	MOV [niedrig], BL		;Kopiere Inhalt in 8-Bit-Variable
	MOV [hoch], BH			;Kopiere Inhalt in 8-Bit-Variable
	MOV AH, 4CH				;Programmende
	INT 21H

SECTION .DATA
variable dw 32ABH
niedrig db 0
hoch db 0
