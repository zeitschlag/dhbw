%macro ausgabe 1
	MOV DX, %1
	MOV AH, 9
	INT 21H
%endmacro

%macro laengencheck 1
	MOV SI, %1
	MOV BX, SI
	
	WEITER:
		CMP byte[SI], '$'
			JE RECHNE
		
		INC SI
		JMP WEITER
		
	RECHNE:
		MOV AX, SI
		SUB AX, BX
		MOV byte[q_len], AL
%endmacro

org 0x100
bits 16

SECTION .CODE
M1:
	laengencheck quelle
	MOV SI, quelle			;Offsets den Indexregistern zuweisen
	MOV DI, ziel
	MOV CL, [q_len]			;Länge beider Strings nach CL
	XOR AL, AL				;AL nullen
	
LOOP1:
	MOV AL, byte[SI]
	MOV BL, byte[DI] 		;Hier mit Schleifentest
	
	CMP AL, BL				;Berechne die Stelle der 1. Ungleichheit
		JNE RAUS
	
	INC SI
	INC DI
	
	LOOP LOOP1				;Schleifenende
	JMP DRUEBER
	
RAUS:
	MOV AL, [q_len]			;Berechne Stelle der 1. Ungleichheit
	SUB AL, CL
	MOV byte[stelle], AL	;Sichere diese Stelle
	INC byte[stelle]		;Inkrementiere diese, da String beim 
	ausgabe formfeed		;Nullten Zeichen anfängt
	ausgabe meldung2		;Ausgaben im Fall der Ungleichheit
	JMP ENDE
	
	ausgabe meldung3
	MOV AL, [stelle]		;Vorbereitung der Ausgabe für die berechnete Stelle
	ADD AL, 30H
	
	;Ziffer am Bildschirm ausgeben
	MOV DL, AL
	MOV AH, 2
	INT 21H
	
	JMP ENDE

DRUEBER:
	ausgabe meldung1		;Wenn Strings identisch, dann Ausgabe
	
ENDE:
	MOV AH, 4CH
	INT 21H

SECTION .DATA
;Ausgabetexte
stelle		db 0	
meldung1	db "Beide Strings sind identisch: ", "$"
meldung2	db "Die beiden Strings sind unterschiedelich: ", "$"
meldung3	db "Unterschied ab Zeichen: $"

formfeed	db 10,13,"$"
blank		db " $"

;Variable zum Fixieren des Vorkommens des 1. Unterschiedes
quelle		db "ein vordefinierter String$"
ziel		db "ein fordefinierter String$"	;Quell- und Zielstrings deklarieren
q_len		db 0