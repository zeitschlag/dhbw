#!/bin/bash

pdflatex main.tex
pdflatex main.tex
pdflatex main.tex

rm main.aux
rm main.idx
rm main.log
rm main.toc
rm chapter/*.aux